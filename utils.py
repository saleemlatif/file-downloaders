"""
Utility functions.
"""
from constants import DOWN_LOADERS
from urllib.parse import urlparse


def get_downloader(url, destination):
    """
    Get the appropriate downloader for given url.

    Arguments:
        url (str): URL pointing to the resource that needs to be downloaded.
        destination (str): Destination directory where downloaded content would be placed.
    Returns:
        (Downloader): Instance of the downloader.
    """
    url_scheme = urlparse(url).scheme
    for downloader in DOWN_LOADERS:
        if url_scheme in downloader.schemes:
            return downloader.downloader_class(url, destination)

    raise NotImplementedError("No downloader found for '{}'.".format(url_scheme))
