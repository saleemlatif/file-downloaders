"""
Tests for Downloader.
"""
import hashlib
import tempfile
import unittest

from pathlib import Path
from unittest.mock import Mock

from downloaders import HttpDownloader as Downloader


class TestDownloader(unittest.TestCase):
    """
    Validate the behavior of Downloader.
    """

    def setUp(self):
        """
        Setup test environment.
        """
        super(TestDownloader, self).setUp()
        self.url = "ftp://example.com/test_files/test_content.bin"
        self.destination = tempfile.gettempdir()
        self.downloader = Downloader(self.url, self.destination)

    def test_error_if_destination_dir_does_not_exist(self):
        """
        Test Downloader initialization fails of destination directory os not valid.
        """
        with self.assertRaises(ValueError):
            Downloader(self.url, "/non/existent/dir")

    def test_file_name(self):
        """
        Validate that file_name property returns the expected string.
        """

        assert self.downloader.file_name == "{hash}-{file_name}".format(
            hash=hashlib.sha1(self.url.encode()).hexdigest(),
            file_name=self.url.split("/")[-1]
        )

    def test_file_path(self):
        """
        Validate that file_path property returns the expected string.
        """
        file_name = "{hash}-{file_name}".format(
            hash=hashlib.sha1(self.url.encode()).hexdigest(),
            file_name=self.url.split("/")[-1]
        )
        assert self.downloader.file_path == str(Path(self.destination) / file_name)

    def test_add_tracker(self):
        """
        Validate the behavior of add_tracker.
        """
        _tracker = Mock()

        self.downloader.add_tracker(_tracker)
        assert _tracker in self.downloader.progress_trackers

    def test_start_trackers(self):
        """
        Validate the behavior of start_trackers.
        """
        mock_tracker = Mock(start=Mock())
        self.downloader.add_tracker(mock_tracker)
        self.downloader.start_trackers(1024)

        mock_tracker.start.assert_called_once_with(1024)
