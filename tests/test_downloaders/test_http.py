"""
Tests for HttpDownloader.
"""
import os
import tempfile
import unittest
from pathlib import Path

from downloaders import http


class TestHttpDownloader(unittest.TestCase):
    """
    Validate the behavior of HttpDownloader.
    """

    def setUp(self):
        """
        Setup test environment.
        """
        super(TestHttpDownloader, self).setUp()
        http.urlopen = self.__mock_urlopen
        self.downloader = http.HttpDownloader(
            "http://example.com/test_files/test_content.bin", tempfile.gettempdir()
        )
        self.test_file = str(Path(__file__).parent / "test_files/test_content.bin")

    def __mock_urlopen(self, *args, **kwargs):
        """
        Mock urlopen for tests.
        """
        req = open(self.test_file, 'rb')
        req.headers = {'content-length': os.path.getsize(self.test_file)}

        self.addCleanup(req.close)
        return req

    def test_download(self):
        """
        Test file download.
        """
        with self.downloader as downloader:
            downloader.download()

        # Verify that new file is created
        assert Path(self.downloader.file_path).exists()
        assert os.path.getsize(self.test_file) == os.path.getsize(self.downloader.file_path)

    def test_partial_downloads_handling(self):
        """
        Verify that file is removed in case of errors during download.
        """
        prev_download = self.downloader.download

        def __mock_download():
            prev_download()

            # verify partial download file was created
            assert Path(self.downloader.file_path).exists()
            raise ValueError("Simulated error in download.")

        with self.assertRaises(ValueError):
            with self.downloader as downloader:
                downloader.download = __mock_download
                downloader.download()

        # Verify that file is removed
        assert not Path(self.downloader.file_path).exists()

    def test_partial_downloads_handling_when_no_partial_file(self):
        """
        Verify that scenario is handled gracefully when partial file is not yet created and download fils.
        """
        def __mock_download():
            # verify partial download file was created
            assert not Path(self.downloader.file_path).exists()
            raise ValueError("Simulated error in download.")

        with self.assertRaises(ValueError):
            with self.downloader as downloader:
                downloader.download = __mock_download
                downloader.download()

        # Verify that file is removed
        assert not Path(self.downloader.file_path).exists()

    def test_unique_file_names_from_url(self):
        """
        Validate that unique file name is calculated from the url.
        """
        first_downloader = http.HttpDownloader(
            "http://example.com/test_files/test_content.bin", tempfile.gettempdir()
        )
        with first_downloader as downloader:
            downloader.download()

        # Download another file with same name but different url path
        second_downloader = http.HttpDownloader(
            "http://example.com/test_files_2/test_content.bin", tempfile.gettempdir()
        )
        with second_downloader as downloader:
            downloader.download()

        # Verify that two new files are created
        assert Path(first_downloader.file_path).exists()
        assert Path(second_downloader.file_path).exists()

        assert first_downloader.file_path != second_downloader.file_path

    def test_http_downloader_init(self):
        """
        Validate downloader initialization method.
        """
        with self.assertRaises(ValueError):
            http.HttpDownloader("http://example.com/test_files/test_content.bin", "/non/existent/directory")
