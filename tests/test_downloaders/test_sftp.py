"""
Tests for SFTPDownloader.
"""
import os
import tempfile
import unittest
from pathlib import Path
from unittest.mock import Mock


from downloaders import sftp


class TestSFTPDownloader(unittest.TestCase):
    """
    Validate the behavior of SFTPDownloader.
    """

    def setUp(self):
        """
        Setup test environment.
        """
        super(TestSFTPDownloader, self).setUp()
        sftp.FTP = MockFTP
        self.downloader = sftp.SFTPDownloader(
            "sftp://user:password@example.com/test_files/test_content.bin", tempfile.gettempdir()
        )
        self.test_file = str(Path(__file__).parent / "test_files/test_content.bin")
        MockFTP.test_file = self.test_file

    def test_get_credentials(self):
        """
        Verify that get_credentials works as expected.
        """
        downloader = sftp.SFTPDownloader(
            "sftp://user:password@example.com/test_files/test_content.bin", tempfile.gettempdir()
        )

        self.assertTupleEqual(
            downloader.get_credentials(),
            ("user", "password"),
        )

    def test_get_credentials_value_error_if_missing_arguments(self):
        """
        Verify that get_credentials raises ValueError if username and password are not provided.
        """
        downloader = sftp.SFTPDownloader(
            "sftp://example.com/test_files/test_content.bin", tempfile.gettempdir()
        )

        with self.assertRaises(ValueError):
            downloader.get_credentials()

    def test_download(self):
        """
        Test file download.
        """
        with self.downloader as downloader:
            downloader.download()

        # Verify that new file is created
        assert Path(self.downloader.file_path).exists()
        assert os.path.getsize(self.test_file) == os.path.getsize(self.downloader.file_path)

    def test_partial_downloads_handling(self):
        """
        Verify that file is removed in case of errors during download.
        """
        prev_download = self.downloader.download

        def __mock_download():
            prev_download()
            raise ValueError("Simulated error in download.")

        with self.assertRaises(ValueError):
            with self.downloader as downloader:
                downloader.download = __mock_download
                downloader.download()

        # Verify that file is removed
        assert not Path(self.downloader.file_path).exists()

    def test_unique_file_names_from_url(self):
        """
        Validate that unique file name is calculated from the url.
        """
        first_downloader = sftp.SFTPDownloader(
            "sftp://user:password@example.com/test_files/test_content.bin", tempfile.gettempdir()
        )
        with first_downloader as downloader:
            downloader.download()

        # Download another file with same name but different url path
        second_downloader = sftp.SFTPDownloader(
            "sftp://user:password@example.com/test_files_2/test_content.bin", tempfile.gettempdir()
        )
        with second_downloader as downloader:
            downloader.download()

        # Verify that two new files are created
        assert Path(first_downloader.file_path).exists()
        assert Path(second_downloader.file_path).exists()

        assert first_downloader.file_path != second_downloader.file_path

    def test_ftp_downloader_init(self):
        """
        Validate downloader initialization method.
        """
        with self.assertRaises(ValueError):
            sftp.SFTPDownloader(
                "sftp://user:password@example.com/test_files/test_content.bin", "/non/existent/directory"
            )


class MockFTP(Mock):
    """
    Mock FTP client.
    """

    # This attribute should be set after mocking the actual module.
    test_file = None

    def login(self, *args, **kwargs):
        """
        Automatically login.
        """
        return True

    def sendcmd(self, cmd):
        """
        Mock out sendcmd
        """

    def size(self, path):
        """
        Mock out size getter.
        """
        return os.path.getsize(self.test_file)

    def retrbinary(self, cmd, callback, blocksize=8192, rest=None):
        """
        Mock out the retrbinary method.
        """
        with open(self.test_file, 'rb') as fp:
            callback(fp.read(blocksize))

