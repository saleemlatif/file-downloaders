"""
Validate the behavior of main.py
"""
import tempfile
import os
import unittest
import hashlib

from collections import namedtuple
from unittest.mock import Mock
from pathlib import Path

import downloaders
import main

from tests.test_downloaders.test_sftp import MockFTP


Arguments = namedtuple('Arguments', "destination urls verbose")


class TestMain(unittest.TestCase):
    """
    Validate the behavior of main.py
    """

    def setUp(self):
        """
        Initialize required attributes.
        """
        self.arguments = Arguments(
            destination=tempfile.gettempdir(),
            urls=["http://example.com/test_files/test_content.bin"],
            verbose=True,
        )
        self.test_file = str(Path(__file__).parent / "test_downloaders/test_files/test_content.bin")

    def test_dispatch_download(self):
        """
        Validate that dispatch_download runs the correct Downloader based on the url/
        """
        downloaders.HttpDownloader.download = Mock()
        downloaders.FTPDownloader.download = Mock()
        downloaders.SFTPDownloader.download = Mock()

        main.dispatch_download("http://example.com/test_files/test_content.bin", tempfile.gettempdir())
        assert downloaders.HttpDownloader.download.called

        main.dispatch_download("ftp://example.com/test_files/test_content.bin", tempfile.gettempdir())
        assert downloaders.FTPDownloader.download.called

        main.dispatch_download("sftp://user@password@example.com/test_files/test_content.bin", tempfile.gettempdir())
        assert downloaders.FTPDownloader.download.called

    def test_main_errors(self):
        """
        Validate the behavior on main function in case of error conditions.
        """
        with self.assertRaises(ValueError):
            main.main(Arguments(destination="/non/existent/dir", urls=[], verbose=True))

        # Make sure Failed message is displayed and exception is suppressed.
        downloaders.HttpDownloader.download = Mock(side_effect=ConnectionError)
        main.sys = Mock(stdout=Mock(write=Mock()))
        main.main(
            Arguments(
                destination=tempfile.gettempdir(),
                urls=["http://example.com/test_files/test_content.bin"],
                verbose=False
            )
        )
        main.sys.stdout.write.assert_called_once_with(
            "\n\033[91mFAILED: \033[00mhttp://example.com/test_files/test_content.bin"
        )

        main.traceback = Mock(print_exc=Mock())

        main.main(
            Arguments(
                destination=tempfile.gettempdir(),
                urls=["http://example.com/test_files/test_content.bin"],
                verbose=True
            )
        )
        assert main.traceback.print_exc.called

    def test_main(self):
        """
        Validate the behavior on main function.
        """
        downloaders.http.urlopen = self.__mock_urlopen
        downloaders.ftp.urlopen = self.__mock_urlopen
        downloaders.sftp.FTP = MockFTP
        MockFTP.test_file = self.test_file

        destination = Path(tempfile.gettempdir())
        http_url = "http://example.com/test_files/test_content.bin"
        https_url = "https://example.com/test_files/test_content_2.bin"
        ftp_url = "ftp://example.com/test_files/test_content_3.bin"
        sftp_url = "sftp://user:pass@example.com/test_files/test_content_3.bin"

        # Verify http downloads
        main.main(
            Arguments(
                destination=str(destination),
                urls=[http_url, https_url, ftp_url, sftp_url],
                verbose=True
            )
        )
        expected_file_name = "{hash}-{file_name}".format(
            hash=hashlib.sha1(http_url.encode()).hexdigest(),
            file_name=http_url.split("/")[-1]
        )
        expected_destination_file = str(destination / expected_file_name)

        # Verify that new file is created
        assert Path(expected_destination_file).exists()
        assert os.path.getsize(self.test_file) == os.path.getsize(expected_destination_file)

        # Verify https downloads
        expected_file_name = "{hash}-{file_name}".format(
            hash=hashlib.sha1(https_url.encode()).hexdigest(),
            file_name=https_url.split("/")[-1]
        )
        expected_destination_file = str(destination / expected_file_name)

        # Verify that new file is created
        assert Path(expected_destination_file).exists()
        assert os.path.getsize(self.test_file) == os.path.getsize(expected_destination_file)

        # Verify ftp downloads
        expected_file_name = "{hash}-{file_name}".format(
            hash=hashlib.sha1(ftp_url.encode()).hexdigest(),
            file_name=ftp_url.split("/")[-1]
        )
        expected_destination_file = str(destination / expected_file_name)

        # Verify that new file is created
        assert Path(expected_destination_file).exists()
        assert os.path.getsize(self.test_file) == os.path.getsize(expected_destination_file)

        # Verify sftp downloads
        expected_file_name = "{hash}-{file_name}".format(
            hash=hashlib.sha1(sftp_url.encode()).hexdigest(),
            file_name=sftp_url.split("/")[-1]
        )
        expected_destination_file = str(destination / expected_file_name)

        # Verify that new file is created
        assert Path(expected_destination_file).exists()
        assert os.path.getsize(self.test_file) == os.path.getsize(expected_destination_file)

    def __mock_urlopen(self, *args, **kwargs):
        """
        Mock urlopen for tests.
        """
        req = open(self.test_file, 'rb')
        req.headers = {'content-length': os.path.getsize(self.test_file)}

        self.addCleanup(req.close)
        return req
