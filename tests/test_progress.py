"""
Tests for progress.py
"""
import unittest

from progress import ConsoleProgressTracker

from io import StringIO
from unittest.mock import patch


class TestConsoleProgressTracker(unittest.TestCase):
    """
    Test that ConsoleProgressTracker works as expected.
    """

    def setUp(self):
        """
        Instantiate required attributes.
        """
        super(TestConsoleProgressTracker, self).setUp()
        self.tracker = ConsoleProgressTracker()

    @patch('sys.stdout', new_callable=StringIO)
    def test_start(self, mock_stdout):
        """
        Verify that start method logs message on the console.
        """
        self.tracker.start(256)
        assert mock_stdout.getvalue() == "Download Started: "
        assert self.tracker.total == 256

    def test_tick_raises_error_when_tracker_not_started(self):
        """
        Verify that call to tick raises ValueError if tracker is not started.
        """
        with self.assertRaises(RuntimeError):
            self.tracker.tick(10)

    @patch('sys.stdout', new_callable=StringIO)
    def test_tick(self, mock_stdout):
        """
        Verify that call to tick updates the progress of tracker.
        """
        self.tracker.start(200)
        self.tracker.tick(10)

        expected_message = "\r[{done}{remaining}]".format(
            done=self.tracker.percentage * "*",
            remaining=(100-self.tracker.percentage) * " ",
        )

        assert self.tracker.done == 10
        assert self.tracker.percentage == 5
        assert expected_message in mock_stdout.getvalue()
