"""
Tests for progress.py
"""
import unittest
from pathlib import Path

from utils import get_downloader
from downloaders import HttpDownloader, FTPDownloader, SFTPDownloader


class TestUtils(unittest.TestCase):
    """
    Test that functions in utils.py work as expected.
    """

    def test_get_downloader(self):
        """
        Verify that call to get_downloader returns correct downloader.
        """
        assert isinstance(
            get_downloader('http://example.com/some-file.txt', Path(__file__).parent),
            HttpDownloader
        )
        assert isinstance(
            get_downloader('https://example.com/some-file.txt', Path(__file__).parent),
            HttpDownloader
        )
        assert isinstance(
            get_downloader('ftp://example.com/some-file.txt', Path(__file__).parent),
            FTPDownloader
        )
        assert isinstance(
            get_downloader('sftp://user:password@example.com/some-file.txt', Path(__file__).parent),
            SFTPDownloader
        )

        with self.assertRaises(NotImplementedError):
            get_downloader("unknown://example.com/some-file.txt", Path(__file__).parent)
