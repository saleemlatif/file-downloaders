from downloaders import HttpDownloader, FTPDownloader, SFTPDownloader
from collections import namedtuple

DOWNLOADER = namedtuple("DOWNLOADER", ["schemes", "downloader_class"])


DOWN_LOADERS = [
    DOWNLOADER({'http', 'https'}, HttpDownloader),
    DOWNLOADER({'ftp'}, FTPDownloader),
    DOWNLOADER({'sftp'}, SFTPDownloader),
]
