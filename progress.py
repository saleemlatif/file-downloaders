import sys
from abc import ABC, abstractmethod


class ProgressTracker(ABC):
    """
    Class to track progress of file download.
    """
    total = None
    done = 0

    def tick(self, tick_amount):
        """
        Update progress.
        """
        if self.total is None:
            raise RuntimeError("Start progress tracker before use.")

        self.done += tick_amount
        self.track_tick()

    def start(self, total):
        """
        Start the progress.
        """
        self.total = int(total or 0)

    @property
    def percentage(self):
        return int((self.done / self.total) * 100 if self.done else 0)

    @abstractmethod
    def track_tick(self):
        """
        This method must be overridden by child classes to customize the tracking of each progress tick.
        """


class ConsoleProgressTracker(ProgressTracker):

    def start(self, total):
        sys.stdout.write("Download Started: ")
        super(ConsoleProgressTracker, self).start(total)

    def track_tick(self):
        sys.stdout.write(self.stdout)

    @property
    def stdout(self):
        """
        Returns a line of string to be printed on sys.stdout
        """
        return "\r[{done}{remaining}]".format(
            done=self.percentage * "*",
            remaining=(100-self.percentage) * " ",
        )
