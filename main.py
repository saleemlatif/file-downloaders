#!/usr/bin/env python

import argparse
import sys
import traceback
from pathlib import Path
from utils import get_downloader


parser = argparse.ArgumentParser(description='Download some files.')
parser.add_argument("-v", "--verbose", help="increase output verbosity",
                    action="store_true")
parser.add_argument('destination', metavar='D', type=str,
                    help='Destination Directory where to save files.')
parser.add_argument('urls', metavar='URL', type=str, nargs='+',
                    help='URL of the file to download.')


def dispatch_download(download_url, destination):
    """
    Dispatch download task to the appropriate downloader.

    Arguments:
        download_url (str): Resource to download.
        destination (str): Destination directory for file downloads.
    """
    with get_downloader(download_url, destination) as downloader:
        downloader.download()


def main(arguments):
    """
    Execute the main script.

    Arguments:
        arguments: command line arguments passed to the script.
    """
    if not Path(arguments.destination).exists():
        raise ValueError("'destination' does not exist.")

    for url in arguments.urls:
        try:
            dispatch_download(url, arguments.destination)
        except Exception as error:
            if arguments.verbose:
                traceback.print_exc()
            sys.stdout.write("\n\033[91mFAILED: \033[00m" + url)
        else:
            sys.stdout.write("\n\033[92mDOWNLOADED: \033[00m" + url)


if __name__ == "__main__":
    main(
        parser.parse_args()
    )
