"""
Downloader for downloading files over ftp.
"""
from .base import Downloader
from urllib.request import urlopen


class FTPDownloader(Downloader):
    def download(self):
        req, chunk = urlopen(self.url), True
        self.start_trackers(int(req.headers['content-length'] or 0))

        with open(self.file_path, 'wb') as fp:
            while chunk:
                chunk = req.read(16384)
                fp.write(chunk)
                self.notify_trackers(len(chunk))
