"""
Downloader for downloading files over http.
"""
from .base import Downloader
from urllib.request import urlopen


class HttpDownloader(Downloader):
    def download(self):
        req, chunk = urlopen(self.url), True
        self.start_trackers(int(req.headers['content-length'] or 0))

        with open(self.file_path, 'wb') as fp:
            while chunk:
                chunk = req.read(8192)
                fp.write(chunk)
                self.notify_trackers(len(chunk))
