from .base import Downloader
from .http import HttpDownloader
from .ftp import FTPDownloader
from .sftp import SFTPDownloader
