"""
Downloader for downloading files over sftp.
"""
from .base import Downloader
from ftplib import FTP
from urllib.parse import urlparse, unquote


class SFTPDownloader(Downloader):
    def download(self):
        url = urlparse(self.url)
        ftp = FTP(url.hostname)
        ftp.login(*self.get_credentials())

        # Set Binary mode
        ftp.sendcmd("TYPE i")
        self.start_trackers(ftp.size(url.path))

        with open(self.file_path, 'wb') as fp:
            def write_callback(chunk):
                fp.write(chunk)
                self.notify_trackers(len(chunk))

            ftp.retrbinary('RETR {file}'.format(file=url.path), write_callback)

    def get_credentials(self):
        """
        Get SFTP username and password from the url.

        Returns:
             (tuple): (username, password) tuple.
        """
        url = urlparse(self.url)
        if not (url.username or url.password):
            raise ValueError("File download over sftp require username and password.")

        return unquote(url.username), unquote(url.password)
