"""
Base Down-loaders.
"""
import os
import hashlib

from abc import ABC, abstractmethod
from pathlib import Path

from progress import ConsoleProgressTracker


class Downloader(ABC):
    """
    Downloader base class.

    Example:
        with Downloader(<url>, <destination>) as downloader:
            destination_file = downloader.downloader()
    """
    progress_trackers = [ConsoleProgressTracker()]

    def __init__(self, url: str, destination: str):
        self.url = url
        self.destination = Path(destination)

        if not self.destination.exists():
            raise ValueError("dir: '{}' does not exist.".format(self.destination))

        super(Downloader, self).__init__()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is not None:
            self._cleanup_on_error()

    @abstractmethod
    def download(self):
        """
        Download the file and return name of the downloaded file.
        """

    def __remove_partial_downloaded_file(self):
        """
        Remove the partially downloaded file in case of error mid download.
        """
        try:
            os.remove(self.file_path)
        except FileNotFoundError:
            # Nothing to remove.
            pass

    def _cleanup_on_error(self):
        """
        Perform cleanup operation in case of errors during file download.
        """
        self.__remove_partial_downloaded_file()

    @property
    def file_name(self):
        """
        Calculate name of the file from given url and return it.
        """
        return "{hash}-{file_name}".format(
            hash=hashlib.sha1(self.url.encode()).hexdigest(),
            file_name=self.url.split("/")[-1]
        )

    @property
    def file_path(self):
        return str(self.destination / self.file_name)

    def add_tracker(self, tracker):
        """
        Add Progress Tacker to the list of trackers.

        Arguments:
            tracker (ProgressTracker): progress tracker to be added.
        """
        self.progress_trackers.append(tracker)

    def notify_trackers(self, chunk_size):
        """
        Notify all trackers about the progress of download.

        Arguments:
            chunk_size (int): Apmount of data downloaded.
        """
        for tracker in self.progress_trackers:
            tracker.tick(chunk_size)

    def start_trackers(self, total_amount):
        """
        Notify all trackers of the download.

        Arguments:
            total_amount (int): Total size of the download.
        """
        for tracker in self.progress_trackers:
            tracker.start(total_amount)
