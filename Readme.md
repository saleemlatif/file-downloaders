Project Structure:
------------------

Project contains the following files and directories.
1. `main.py`
	> This is the entry point for down-loaders. It is a python executable that will instantiate and start down-loaders. 
	You can run it like `python main.py <destination> <url> <url> ...`
2. `constants.py` 
	> As the name implies, it is a container for all the constants used throughout.
3. `progress.py`
	> It contains base `ProgressTracker` and `ConsoleProgressTracker` to track the progress of individual downloads.
4. `utils.py`
	> This file is a container of utility functions for the module.
5. `downloaders/`
	> Directory to hold down-loaders for different protocols like `http`, `ftp` etc. 
	If new protocols are to be supported, they should be added in this directory.
	1. `base.py`
		> File containing base `Downloader` that acts as the base class for all the down-loaders.
	2. `http.py`
		> It contains `HttpDownloader` that is used to download files over http.
	3. `ftp.py`
		> It contains `FTPDownloader` to download files over ftp protocol.
	4. `sftp.py`
		> It contains `SFTPDownloader` to download files over sftp protocol.
5. `tests/`
	> Directory to hold all the tests. You can run tests using `python -m unittest`. 
	Sub directories are pretty much self explanatory and contains test for each file.

Pre-Requisites:
---------------
Code is written in `Python 3` and it is the only requirement to run the code. I have written the functionality 
without using any third party packages and using only the features python provides natively.

How to Run?
-----------
Open any command line tool of your choice, `cd` to the folder containing `main.py` of this project and 
execute the following command

```bash
python main.py <destination> <url1> <url2> <url3> ... -v
```
- `<destination>` is the directory where you want to store the downloaded contents.
- `<url1> <url2> <url3> ...` After the first parameter comes a space separated list of urls that you want to download.
- `-v` this flag is used to add verbosity to your command. Omit this if you want the standard output.

Example:
--------
```shell
python main.py /var/downloads/ https://speed.hetzner.de/100MB.bin 
```

How to Test?
------------
You can run the test by running the following command inside the project directory
```shell
python -m unittest -v
```

How to Extend?
--------------
Currently the support for protocols is limited to `http`, `https`, `ftp` and `sftp`, however, you can extend the list of 
supported procols by extending the down-loaders. Here are the steps you need to do just that

1. Create a file in `downloaders/` directory, you can name it anyway you want, but a preferred way is to name it the 
same as the protocol it supports like `http`, `ftp` etc.
2. In the file you created, create a class with base class `Downloader` from `downloaders/base.py`. 
The only method required to be overridden is `download`. It should take no arguments (apart from `self` of course.)
This method should perform the following tasks
	1. Method should be implemented keeping in mind large file sizes, so use streaming for incremental downloads. 
	2. It should start the trackers by calling `self.start_trackers`, total size of the file to download should be 
	passed as an `int` as the argument to this method. 
	3. Take the `self.url` and download the file to `self.file_path`.  
	4. Call `self.notify_trackers` whenever a chunk of data is downloaded, size of the chunk as an `int` should be 
	passed as the argument.
3. (Optional) Import the new downloader class in `downloaders/__init__.py` to conform the convention used overall in this project.
4. Add your Downloader class to `DOWN_LOADERS` in `contants.py`
	> `DOWN_LOADERS` is a list of `namedtuple` i.e. `DOWNLOADER` defined in `constants.py`. It has two items, 
	first on is a set of protocols you downloader handles. and second is the reference to the class of your downloader.

That's all, you should now be able to download your files over the new protocol.
 
